// console.log("Hello World");



let getNumber = {
	num: 2
}

let getCube = `The cube of ${getNumber.num} is ${2 ** 3}`;
console.log(getCube);


let address = [ "Washington Ave NW", "California", 258, 90011];

let myAddress1 = address[0];
let myAddress2 = address[1];
let myAddress3 = address[2];
let myAddress4 = address[3];

let myCompleteAddress = `I live at ${myAddress3} ${myAddress1}, ${myAddress2} ${myAddress4}`;
console.log(myCompleteAddress);

let animal = {
	name: 'Lolong',
	type: 'Saltwater crocodile',
	weight: 1075,
	height: 20,
	width: 3,
}

let myAnimal = `${animal.name} was a ${animal.type}. He weighed at ${animal.weight} kgs with a measurement of ${animal.height} ft ${animal.width} in`;
console.log(myAnimal);

let numArray = [1, 2, 3, 4, 5];

let numA;
for (numA = 1; numA < 6; numA++){
	console.log(numA);
}

let reducedNumArray = numArray.reduce((x, y) => {
	return x + y;
})
console.log(reducedNumArray);

class Dog {

	constructor(name) {
		this.name = "Frankie";
		this.age = 5;
		this.breed = "Miniature Dachshund"
	}
};

let Dog1 = new Dog("Frankie", "5", "Miniature Dachshund");
console.log(Dog1);


// In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
// Link the index.js file to the index.html file.
// Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
// Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
// Create a variable address with a value of an array containing details of an address.
// Destructure the array and print out a message with the full address using Template Literals.
// Create a variable animal with a value of an object data type with different animal details as it’s properties.
// Destructure the object and print out a message with the details of the animal using Template Literals.
// Create an array of numbers.
// Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
// Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
// Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// Create/instantiate a new object from the class Dog and console log the object.
// Create a git repository named S24.
// Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// Add the link in Boodle.